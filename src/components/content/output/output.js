import { Component } from "react";

import like from "../../../assets/images/like.png";

class Output extends Component {
    render() {
        return (
            <>
                <div className="pt-4">
                    <p>Thông điệp ở đây</p>
                </div>
                <div>
                    <img src={like} alt="like" width={100} />
                </div>
            </>
        )
    }
}

export default Output;