import { Component } from "react";
import Input from "./input/input";
import Output from "./output/output";

class Content extends Component {
    render() {
        return (
            <>
                <Input />
                <Output />
            </>
        )
    }
}

export default Content;