import { Component } from "react";
import Title from "./title/title";
import Background from "./background/background";

class Header extends Component {
    render() {
        return (
            <>
                <Title />
                <Background />
            </>
        )
    }
}

export default Header;